Java Intervals
==============

<p>This repository has moved to <a href="https://codeberg.org/Bobulous/JavaIntervals">Codeberg</a>. This GitLab copy has been archived (read-only mode). To contribute to this project, please go to the <a href="https://codeberg.org/Bobulous/JavaIntervals">CodeBerg repository</a>.</p>
